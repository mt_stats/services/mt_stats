defmodule MtStats.Repo.Migrations.AddStatsAggregates do
  use Ecto.Migration

  @buckets [{"1 day", "day"}, {"1 hour", "hour"}, {"30 minutes", "30m"}]

  def up do
    for {ts, bucket} <- @buckets do
      execute """
      CREATE MATERIALIZED VIEW server_stats_timescale_#{bucket}_aggr
      WITH (timescaledb.continuous) AS
      SELECT
        time_bucket('#{ts}', "inserted_at") AS time,
          server_id,
          max(clients) AS clients_high,
          first(clients, inserted_at) AS clients_open,
          last(clients, inserted_at) AS clients_close,
          min(clients) AS clients_low,
          avg(clients) AS clients_avg,
          stddev_pop(clients) AS clients_std,
          max(ping) AS ping_high,
          first(ping, inserted_at) AS ping_open,
          last(ping, inserted_at) AS ping_close,
          min(ping) AS ping_low,
          avg(ping) AS ping_avg,
          stddev_pop(ping) AS ping_std
      FROM server_stats_timescale srt
      GROUP BY time, server_id
      WITH NO DATA
      """

      execute """
      SELECT add_continuous_aggregate_policy('server_stats_timescale_#{bucket}_aggr',
        start_offset => INTERVAL '3 days',
        end_offset => INTERVAL '1 hour',
        schedule_interval => INTERVAL '1 days');
      """
    end
  end

  def down do
    for {_ts, bucket} <- @buckets do
      execute "DROP MATERIALIZED VIEW server_stats_timescale_#{bucket}_aggr"
    end
  end
end
