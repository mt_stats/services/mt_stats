defmodule MtStats.Repo.Migrations.AddServerStatsData do
  use Ecto.Migration

  def change do
    create table(:servers) do
      add :name, :string
      add :description, :text
      add :privs, :string
      add :gameid, :string
      add :ip, :string
      add :address, :string, null: false
      add :port, :integer, null: false
      add :version, :string
      add :geo_continent, :string
      timestamps()
    end
    create unique_index(:servers, [:address, :port])

    create table(:players) do
      add :name, :string, null: false
      timestamps()
    end
    create unique_index(:players, [:name])

    create table(:server_stats) do
      add :server_id, references(:servers, on_delete: :delete_all), null: false
      add :update_time, :integer
      add :lag, :float
      add :uptime, :integer
      add :ping, :float
      add :pop_v, :float # good
      add :start, :integer
      add :clients, :integer # good
      add :game_time, :integer
      add :updates, :integer # good
      timestamps()
    end
    
    create table(:player_sessions) do
      add :player_id, references(:players, on_delete: :delete_all), null: false
      add :server_id, references(:servers, on_delete: :delete_all), null: false
      add :from, :utc_datetime, null: false
      add :to, :utc_datetime, null: false
    end
  end
end
