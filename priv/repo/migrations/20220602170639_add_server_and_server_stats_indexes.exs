defmodule MtStats.Repo.Migrations.AddServerAndServerStatsIndexes do
  use Ecto.Migration

  def change do
    create index(:server_stats, [:server_id, :inserted_at])
  end
end
