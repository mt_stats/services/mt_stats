defmodule MtStats.Repo.Migrations.SwitchToTimescaldbHypertable do
  use Ecto.Migration.Timescaledb
  import Ecto.Query, only: [from: 2]
  alias MtStats.Repo

  @data_import_chunk_size 5000

  def up do
    execute "CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE"

    create table(:server_stats_timescale, primary_key: false) do
      add :server_id, references(:servers, on_delete: :delete_all), primary_key: true
      add :inserted_at, :utc_datetime, primary_key: true
      add :ping, :float, null: false
      add :clients, :integer, null: false # good
    end

    create_hypertable(:server_stats_timescale, :inserted_at)
    add_dimension(:server_stats_timescale, :server_id, number_partitions: 4)

    flush()
    # Now import data from old table
    query = from s in "server_stats",
      select: [:server_id, :inserted_at, :ping, :clients]
    
    query
    |> Repo.stream(max_rows: @data_import_chunk_size)
    |> Stream.chunk_every(@data_import_chunk_size)
    |> Stream.each(fn data ->
      # There shouldn't be conflicts, and yet there are... meaning somewhere we screwed up...
      # probably timezones?
      # musttest.minecity.online:3000 appears 3 times in server list, so ignore it
      Repo.insert_all("server_stats_timescale", data, on_conflict: :nothing)
    end)
    |> Stream.run()
  end

  def down do
    drop(table(:server_stats_timescale))
    execute "DROP EXTENSION IF EXISTS timescaledb CASCADE"
  end
end
