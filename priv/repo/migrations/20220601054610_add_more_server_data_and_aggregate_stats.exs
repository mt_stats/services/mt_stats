defmodule MtStats.Repo.Migrations.AddMoreServerDataAndAggregateStats do
  use Ecto.Migration

  def change do
    alter table(:servers) do
      ## extra data
      add :url, :string
      add :creative, :boolean
      add :damage, :boolean
      add :pvp, :boolean

      ## aggregates
      add :ping_current, :float
      add :pop_v_current, :float
      add :clients_current, :integer
      
      add :ping_max, :float
      add :pop_v_max, :float
      add :clients_max, :integer
      
      add :ping_avg, :float
      add :pop_v_avg, :float
      add :clients_avg, :integer

      add :ping_count, :integer
      add :pop_v_count, :integer
      add :clients_count, :integer
    end
  end
end
