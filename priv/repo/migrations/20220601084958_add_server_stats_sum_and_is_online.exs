defmodule MtStats.Repo.Migrations.AddServerStatsSumAndIsOnline do
  use Ecto.Migration

  def change do
    alter table(:servers) do
      add :is_online, :boolean

      add :ping_sum, :float
      add :pop_v_sum, :float
      add :clients_sum, :integer
    end
  end
end
