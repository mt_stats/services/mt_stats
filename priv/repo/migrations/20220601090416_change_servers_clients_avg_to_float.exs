defmodule MtStats.Repo.Migrations.ChangeServersClientsAvgToFloat do
  use Ecto.Migration

  def change do
    alter table(:servers) do
      modify :clients_avg, :float
    end
  end
end
