defmodule MtStats.ListConsumer do
  use GenStage
  require Logger

  def start_link(_args) do
    initial_state = %{servers: %{}}
    GenStage.start_link(__MODULE__, initial_state)
  end

  def init(initial_state) do
    sub_opts = [{MtStats.ListProducer, min_demand: 0, max_demand: 1}]
    {:consumer, initial_state, subscribe_to: sub_opts}
  end

  def handle_events(events, _from, state) do
    servers =
      Enum.map(events, fn server ->
        if Map.has_key?(state.servers, server["address"]) do
          MtStats.get_server_stats(server, state.servers[server["address"]])
        else
          MtStats.get_server_stats(server, nil)
        end

        {server["address"], server}
      end)
      |> Enum.into(%{})

    {:noreply, [], %{state | servers: servers}}
  end
end
