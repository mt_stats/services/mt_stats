defmodule MtStats.Release do
  @moduledoc """
  Used for executing DB release tasks when run in production without Mix
  installed.
  """
  @app :mt_stats

  def migrate do
    load_app()

    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    load_app()
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  def refresh do
    load_app()

    for repo <- repos(),
        bucket <- ["day", "hour", "30m"] do
      repo.query("""
      CALL refresh_continuous_aggregate(
        'server_stats_timescale_#{bucket}_aggr',
        now()::timestamp - INTERVAL '1 month',
        now()::timestamp
        );
      """)
    end
  end

  defp repos do
    Application.fetch_env!(@app, :ecto_repos)
  end

  defp load_app do
    Application.load(@app)
  end
end
