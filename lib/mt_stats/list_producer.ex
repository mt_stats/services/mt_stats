defmodule MtStats.ListProducer do
  use GenStage
  require Logger

  @list_address "http://servers.minetest.net/list"

  # In milliseconds
  @scrape_interval 30_000
  @retry_interval 5 * 60_000

  def start_link(_args) do
    initial_state = []
    GenStage.start_link(__MODULE__, initial_state, name: __MODULE__)
  end

  def init(initial_state) do
    send(self(), :scrape)

    {:producer, initial_state}
  end

  def handle_demand(_demand, state) do
    events = []
    {:noreply, events, state}
  end

  def handle_info(:scrape, state) do
    events = scrape()

    {:noreply, events, state}
  end

  def scrape() do
    with {:http, {:ok, response}} <- {:http, HTTPoison.get(@list_address)},
         {:json, {:ok, data}} <- {:json, Jason.decode(response.body)},
         {:list, %{"list" => list}} <- {:list, data} do
      Logger.info("Checked serverlist successfully")
      Process.send_after(self(), :scrape, @scrape_interval)
      list
    else
      {:http, {:error, error}} ->
        Logger.error("HTTP Error: #{inspect(error)}")
        Process.send_after(self(), :scrape, @retry_interval)
        []

      {:json, {:error, error}} ->
        Logger.error("Json Parse Error: #{inspect(error)}")
        Process.send_after(self(), :scrape, @retry_interval)
        []

      {:list, _data} ->
        Logger.error("Failed to find serverlist in json data")
        Process.send_after(self(), :scrape, @retry_interval)
        []
    end
  end
end
