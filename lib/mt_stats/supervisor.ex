defmodule MtStats.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    children = [
      MtStats.Repo
      #      MtStats.ListProducer,
      #      MtStats.ListConsumer
    ]

    opts = [strategy: :one_for_one, name: MtStats.Supervisor]
    Supervisor.init(children, opts)
  end
end
