defmodule MtStats.Repo do
  use Ecto.Repo,
    otp_app: :mt_stats,
    adapter: Ecto.Adapters.Postgres
end
