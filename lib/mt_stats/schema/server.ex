defmodule MtStats.Server do
  use Ecto.Schema
  import Ecto.Changeset

  schema "servers" do
    field(:name, :string)
    field(:description, :string)
    field(:privs, :string)
    field(:gameid, :string)
    field(:ip, :string)
    field(:address, :string)
    field(:port, :integer)
    field(:version, :string)
    field(:geo_continent, :string)
    field(:url, :string)
    field(:creative, :boolean)
    field(:damage, :boolean)
    field(:pvp, :boolean)

    field(:is_online, :boolean)

    # aggregates
    field(:ping_current, :float)
    field(:pop_v_current, :float)
    field(:clients_current, :integer)

    field(:ping_max, :float)
    field(:pop_v_max, :float)
    field(:clients_max, :integer)

    field(:ping_avg, :float)
    field(:pop_v_avg, :float)
    field(:clients_avg, :float)

    field(:ping_count, :integer)
    field(:pop_v_count, :integer)
    field(:clients_count, :integer)

    field(:ping_sum, :float)
    field(:pop_v_sum, :float)
    field(:clients_sum, :integer)

    has_many(:server_stat, MtStats.ServerStatTimescale)
    has_many(:player_session, MtStats.PlayerSession)
    timestamps()
  end

  def changeset(server, params) do
    server
    |> cast(params, [
      :name,
      :description,
      :privs,
      :gameid,
      :ip,
      :address,
      :port,
      :version,
      :geo_continent,
      :url,
      :creative,
      :damage,
      :pvp,
      :is_online,
      :ping_current,
      :pop_v_current,
      :clients_current,
      :ping_max,
      :pop_v_max,
      :clients_max,
      :ping_avg,
      :pop_v_avg,
      :clients_avg,
      :ping_count,
      :pop_v_count,
      :clients_count,
      :ping_sum,
      :pop_v_sum,
      :clients_sum
    ])
    |> validate_required([:address, :port])
  end
end
