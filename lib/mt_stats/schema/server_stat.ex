defmodule MtStats.ServerStat do
  use Ecto.Schema
  import Ecto.Changeset

  schema "server_stats" do
    belongs_to(:server, MtStats.Server)
    field(:update_time, :integer)
    field(:lag, :float)
    field(:uptime, :integer)
    field(:ping, :float)
    field(:pop_v, :float)
    field(:start, :integer)
    field(:clients, :integer)
    field(:game_time, :integer)
    field(:updates, :integer)
    timestamps()
  end

  def changeset(stat, params) do
    stat
    |> cast(params, [
      :update_time,
      :lag,
      :uptime,
      :ping,
      :pop_v,
      :start,
      :clients,
      :game_time,
      :updates
    ])
  end
end
