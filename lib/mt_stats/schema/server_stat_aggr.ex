defmodule MtStats.ServerStatAggr do
  @buckets [
    {"1 day", "day", MtStats.ServerStatAggr.Day},
    {"1 hour", "hour", MtStats.ServerStatAggr.Hour},
    {"30 minutes", "30m", MtStats.ServerStatAggr.HalfHour}
  ]

  def buckets(), do: @buckets

  def module_options() do
    [MtStats.ServerStatAggr.Instant | Enum.map(@buckets, fn {_, _, x} -> x end)]
  end

  defmodule Instant do
    use Ecto.Schema

    @primary_key false
    schema "server_stats_timescale" do
      belongs_to(:server, MtStats.Server)
      field(:clients_high, :integer, source: :clients)
      field(:clients_first, :integer, source: :clients)
      field(:clients_last, :integer, source: :clients)
      field(:clients_min, :integer, source: :clients)
      field(:clients_avg, :decimal, source: :clients)
      field(:clients_std, :decimal, source: :clients)
      field(:ping_high, :decimal, source: :ping)
      field(:ping_first, :decimal, source: :ping)
      field(:ping_last, :decimal, source: :ping)
      field(:ping_min, :decimal, source: :ping)
      field(:ping_avg, :decimal, source: :ping)
      field(:ping_std, :decimal, source: :ping)
      field(:time, :utc_datetime, source: :inserted_at)
    end
  end

  for {_ts, bucket, module} <- @buckets do
    defmodule module do
      use Ecto.Schema

      # just ignore this thing!
      @primary_key false
      schema "server_stats_timescale_#{bucket}_aggr" do
        belongs_to(:server, MtStats.Server)
        field(:clients_high, :integer)
        field(:clients_first, :integer)
        field(:clients_last, :integer)
        field(:clients_min, :integer)
        field(:clients_avg, :decimal)
        field(:clients_std, :decimal)
        field(:ping_high, :decimal)
        field(:ping_first, :decimal)
        field(:ping_last, :decimal)
        field(:ping_min, :decimal)
        field(:ping_avg, :decimal)
        field(:ping_std, :decimal)
        timestamps(inserted_at: :time, updated_at: false, type: :utc_datetime)
      end
    end
  end
end
