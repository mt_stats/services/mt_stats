defmodule MtStats.Types.Range do
  use Ecto.Type
  # just convert to seconds
  def type, do: :integer

  def cast(range) when is_binary(range) do
    with {num, unit} <- Float.parse(range),
         {:ok, mult_by} <- to_sec(unit) do
      {:ok, ceil(num * mult_by)}
    else
      _ -> :error
    end
  end

  def cast(range) when is_integer(range), do: {:ok, range}
  def cast(_), do: :error

  def cast!(range) do
    {:ok, range} = cast(range)
    range
  end

  def load(data) when is_integer(data) do
    {:ok, data}
  end

  def dump(range) when is_integer(range), do: {:ok, range}
  def dump(_), do: :error

  # Helpers

  defp to_sec(unit) do
    unit |> String.trim() |> String.downcase() |> to_sec_h()
  end

  defp to_sec_h("s" <> _), do: {:ok, 1}
  defp to_sec_h("mo" <> _), do: {:ok, 60 * 60 * 24 * 7 * 4}
  defp to_sec_h("m" <> _), do: {:ok, 60}
  defp to_sec_h("h" <> _), do: {:ok, 60 * 60}
  defp to_sec_h("d" <> _), do: {:ok, 60 * 60 * 24}
  defp to_sec_h("w" <> _), do: {:ok, 60 * 60 * 24 * 7}
  defp to_sec_h("y" <> _), do: {:ok, 60 * 60 * 24 * 7 * 4 * 12}
  defp to_sec_h(_), do: :error
end
