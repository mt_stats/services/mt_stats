defmodule MtStats.Types.Resolution do
  use Ecto.Type
  alias MtStats.ServerStatAggr
  def type, do: :string

  for {_ts, bucket, module} <- ServerStatAggr.buckets() do
    def cast(unquote(bucket)), do: {:ok, unquote(module)}
    def cast(unquote(module)), do: {:ok, unquote(module)}
  end

  def cast(_), do: :error

  for {_ts, bucket, module} <- ServerStatAggr.buckets() do
    def load(unquote(bucket)), do: {:ok, unquote(module)}
  end

  for {_ts, bucket, module} <- ServerStatAggr.buckets() do
    def dump(unquote(module)), do: {:ok, unquote(bucket)}
  end

  def dump(_), do: :error
end
