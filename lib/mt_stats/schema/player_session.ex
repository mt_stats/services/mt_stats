defmodule MtStats.PlayerSession do
  use Ecto.Schema

  schema "player_sessions" do
    belongs_to(:player, MtStats.Player)
    belongs_to(:server, MtStats.Server)
    field(:from, :utc_datetime)
    field(:to, :utc_datetime)
  end
end
