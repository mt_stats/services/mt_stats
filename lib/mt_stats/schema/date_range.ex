defmodule MtStats.DateRange do
  @moduledoc """
  Date + Range interval selection for specific dataset
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias MtStats.{Server, Types.Range, DateRange, ServerStatAggr}
  import Ecto.Query, only: [from: 2]

  @primary_key false
  embedded_schema do
    belongs_to(:server, Server)
    field(:range, Range)
    field(:date, :utc_datetime)
    field(:resolution, Ecto.Enum, values: ServerStatAggr.module_options())
  end

  @min_range "2 minutes"
  @max_range "50 years"
  def changeset(change, params) do
    change
    |> cast(params, [:server_id, :range, :date])
    # now() if no date
    |> validate_required([:server_id, :range])
    |> validate_number(:range,
      greater_than_or_equal_to: Range.cast!(@min_range),
      message: "must be >= #{@min_range}"
    )
    |> validate_number(:range,
      less_than_or_equal_to: Range.cast!(@max_range),
      message: "must be <= #{@max_range}"
    )
    |> validate_date_available()
    |> determine_resolution()
  end

  defp validate_date_available(change) do
    validate_change(change, :date, fn :date, date ->
      near_future = DateTime.add(DateTime.utc_now(), Range.cast!("2d"), :second)
      data_begins = ~U[2022-01-01 00:00:00Z]

      cond do
        DateTime.compare(date, near_future) == :gt ->
          [date: "too far in the future"]

        DateTime.compare(date, data_begins) == :lt ->
          [date: "too far in the past"]

        true ->
          []
      end
    end)
  end

  defp determine_resolution(%{valid?: true} = change) do
    # in seconds
    range = get_change(change, :range)
    # 5000 measurement max (roughly)
    # 1 measurement per 30 seconds
    # 
    resolution =
      cond do
        range <= Range.cast!("2 days") ->
          MtStats.ServerStatAggr.Instant

        range <= Range.cast!("15 days") ->
          MtStats.ServerStatAggr.HalfHour

        range <= Range.cast!("1 month") ->
          MtStats.ServerStatAggr.Hour

        true ->
          MtStats.ServerStatAggr.Day
      end

    change(change, resolution: resolution)
  end

  defp determine_resolution(change), do: change

  def try_params(params) do
    %DateRange{}
    |> changeset(params)
    |> apply_action(:insert)
  end

  def default!(server_id) do
    {:ok, default} = try_params(%{server_id: server_id, range: "1 week"})
    default
  end

  def build_query(%DateRange{} = date_range) do
    date =
      if date_range.date do
        date_range.date
      else
        DateTime.utc_now()
      end

    from_time = DateTime.add(date, -date_range.range, :second)

    from(s in date_range.resolution,
      where:
        s.server_id == ^date_range.server_id and
          s.time > ^from_time,
      order_by: s.time,
      select: [:time, :ping_avg, :clients_avg]
    )
  end
end
