defmodule MtStats.Player do
  use Ecto.Schema

  schema "players" do
    field(:name, :string)
    has_many(:player_sessions, MtStats.PlayerSession)
    timestamps()
  end
end
