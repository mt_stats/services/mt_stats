defmodule MtStats.ServerStatTimescale do
  use Ecto.Schema
  import Ecto.Changeset

  # just ignore this thing!
  @primary_key false
  schema "server_stats_timescale" do
    belongs_to(:server, MtStats.Server)
    field(:ping, :float)
    field(:clients, :integer)
    timestamps(updated_at: false, type: :utc_datetime)
  end

  def changeset(stat, params) do
    stat
    |> cast(params, [:ping, :clients])
  end
end
