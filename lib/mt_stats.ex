defmodule MtStats do
  require Logger

  alias MtStats.{Repo, Server, ServerStatTimescale}
  import Ecto.Changeset

  def add_server(server_params) do
    server_change = Server.changeset(%Server{}, server_params)

    Repo.insert(
      server_change,
      on_conflict: {
        :replace_all_except,
        [:id, :address, :port, :inserted_at]
      },
      conflict_target: [:address, :port]
    )
  end

  def add_stat(server, stats_params) do
    # should be no conflict here
    server
    |> Ecto.build_assoc(:server_stat)
    |> ServerStatTimescale.changeset(stats_params)
    |> Repo.insert(on_conflict: :nothing)

    # again, some servers appear multiple number of times for some reason
  end

  def add_server_aggregates(server, stat) do
    Server.changeset(
      server,
      %{
        ping_current: stat.ping,
        # pop_v_current: stat.pop_v,
        clients_current: stat.clients,
        ping_max: max(server.ping_max || 0, stat.ping || 0),
        # pop_v_max: max(server.pop_v_max || 0, stat.pop_v || 0),
        clients_max: max(server.clients_max || 0, stat.clients || 0),
        ping_avg: (server.ping_sum || 0) / (server.ping_count || 1),
        # pop_v_avg: (server.pop_v_sum || 0) / (server.pop_v_count || 1),
        clients_avg: (server.clients_sum || 0) / (server.clients_count || 1),
        ping_count: (server.ping_count || 0) + ((stat.ping && 1) || 0),
        # pop_v_count: (server.pop_v_count || 0) + (stat.pop_v && 1 || 0),
        clients_count: (server.clients_count || 0) + ((stat.clients && 1) || 0),
        ping_sum: (server.ping_sum || 0) + (stat.ping || 0),
        # pop_v_sum: (server.pop_v_sum || 0) + (stat.pop_v || 0),
        clients_sum: (server.clients_sum || 0) + (stat.clients || 0),
        is_online: true
      }
    )
    |> Repo.update()
  end

  # Compute stats all the way from the start
  def refresh_stats_fully() do
  end

  def get_server_stats(server_update, _nil) do
    with {:ok, server} <- add_server(server_update),
         {:ok, stat} <- add_stat(server, server_update),
         {:ok, server} <- add_server_aggregates(server, stat) do
      Logger.info("Processing #{server.name}")
    else
      x -> Logger.error("Error: #{x}")
    end
  end

  def get_server_stats(server, prev_data) do
    []
  end
end
