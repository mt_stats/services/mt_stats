{:ok, response} = HTTPoison.get("http://servers.minetest.net/list")
{:ok, json} = Jason.decode(response.body)
list = json["list"]
server = List.first(list)

# Repo stuff
import Ecto.Query, only: [from: 2]

alias MtStats.Repo
alias MtStats.{Server, ServerStat}

stats_for = fn x ->
  Repo.all(from stat in ServerStat,
    join: server in Server,
    on: server.id == stat.server_id,
    where: server.id == ^x,
    order_by: [desc: stat.clients],
    select: {server.id, server.address, server.port, stat.clients}
  )
end
