import Config

config :mt_stats,
  ecto_repos: [MtStats.Repo]

import_config "#{Mix.env()}.exs"
