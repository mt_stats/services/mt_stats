import Config

# Configure your database
config :mt_stats, MtStats.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "mt_stats_dev",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

config :logger, :console, format: "[$level] $message\n"
