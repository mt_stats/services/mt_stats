import Config

config :mt_stats, MtStats.Repo,
  username: "postgres",
  password: "postgres",
  hostname: "localhost",
  database: "mt_stats_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

config :logger, level: :warn
