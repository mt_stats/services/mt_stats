defmodule MtStats.MixProject do
  use Mix.Project

  def project do
    [
      app: :mt_stats,
      version: "0.1.0",
      elixir: "~> 1.13",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      aliases: aliases()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # stage data processing
      {:gen_stage, "~> 1.0"},
      # http
      {:httpoison, "~> 1.8"},
      # json
      {:jason, "~> 1.3"},
      # persistant storage with associations
      {:ecto_sql, "~> 3.7.1"},
      {:postgrex, ">= 0.0.0"},
      {:ecto_psql_extras, "~> 0.7.4"},
      {:ecto_timescaledb, "~> 0.10.0"}
      # {:ecto_sqlite3, "~> 0.7.2"}, # sqlite3 embedded storage driver
      # {:asciichart, "~> 1.0"}, # charts in terminal!
    ]
  end

  defp aliases do
    [
      # , "run priv/repo/seeds.exs"],
      "ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.reset": ["ecto.drop", "ecto.setup"]
    ]
  end
end
